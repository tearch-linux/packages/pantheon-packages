
# Unibuild files for Pantheon packages.

***Use unibuild to building packages!***


## Packages (pantheon-tearch):

### General
- granite
- contractor
- gala
- pantheon-polkit-agent
- pantheon-geoclue2-agent
- pantheon-notifications
- pantheon-session
- pantheon-settings-daemon
- capnet-assist
- xdg-desktop-portal-pantheon
- touchegg
- pantheon-print
- pantheon-fixes

### Switchboard
- switchboard
- switchboard-plug-about
- switchboard-plug-power
- switchboard-plug-applications
- switchboard-plug-parental-controls
- switchboard-plug-datetime
- switchboard-plug-notifications
- switchboard-plug-mouse-touchpad
- switchboard-plug-desktop
- switchboard-plug-display
- switchboard-plug-bluetooth
- switchboard-plug-sharing
- switchboard-plug-sound
- switchboard-plug-elementary-tweaks
- switchboard-plug-user-accounts
- switchboard-plug-a11y
- switchboard-plug-network
- switchboard-plug-keyboard
- switchboard-plug-printers
- switchboard-plug-online-accounts
- switchboard-plug-wacom
- switchboard-plug-locale

### Wingpanel and Appmenu
- wingpanel
- pantheon-applications-menu
- wingpanel-indicator-bluetooth
- wingpanel-indicator-nightlight
- wingpanel-indicator-sound
- wingpanel-indicator-power
- wingpanel-indicator-network
- wingpanel-indicator-notifications
- wingpanel-indicator-datetime
- wingpanel-indicator-keyboard
- wingpanel-indicator-a11y
- wingpanel-indicator-namarupa
- wingpanel-indicator-session


## Packages (pantheon-extra-tearch):

### Core Applications
- pantheon-code
- pantheon-files
- pantheon-music
- pantheon-photos
- pantheon-screencast
- pantheon-shortcut-overlay
- pantheon-terminal
- pantheon-videos
- pantheon-calculator
- pantheon-calendar
- pantheon-camera
- pantheon-screenshot
- pantheon-tasks
- pantheon-mail
- pantheon-sideload
- pantheon-appcenter

### Extra Applications
- [appeditor](https://appcenter.elementary.io/com.github.donadigo.appeditor/)
- [desktopfolder](https://appcenter.elementary.io/com.github.spheras.desktopfolder/)
- [planner](https://appcenter.elementary.io/com.github.alainm23.planner/)

### Miscellaneous
- gtk-theme-elementary
- elementary-wallpapers
- elementary-icon-theme